package me.teeage.wardrobe;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.teeage.wardrobe.util.Item;
import me.teeage.wardrobe.util.Metrics;

public class Wardrobe extends JavaPlugin {

	private static Wardrobe instance;
	private int itemId;
	private int itemData;
	private int itemSlot;
	private String itemName;
	private List<String> worlds;

	@Override
	public void onEnable() {
		instance = this;
		check("Item.id", 402);
		check("Item.data", 0);
		check("Item.name", "&eWardrobe");
		check("Item.slot", 8);
		check("inv1Name", "&eWardrobe - Page 1");
		check("inv2Name", "&eWardrobe - Page 2");
		check("Worlds", Arrays.asList("world", "test"));
		check("signLineOne", "&7*********");
		check("signLineTwo", "&aWardrobe");
		check("signLineThree", "&eclick to open");
		check("signLineFour", "&7*********");
		itemId = getConfig().getInt("Item.id");
		itemData = getConfig().getInt("Item.data");
		itemSlot = getConfig().getInt("Item.slot");
		itemName = getConfig().getString("Item.name");
		worlds = getConfig().getStringList("Worlds");
		Bukkit.getPluginManager().registerEvents(new WardrobeListener(), this);
		WardrobeManager.init();
		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void check(String path, Object value) {
		if (!getConfig().contains(path)) {
			getConfig().set(path, value);
			saveConfig();
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("wardrobe")) {
			if (sender instanceof Player) {
				Player p = (Player) sender;
				if (args.length == 0) {
					if (p.hasPermission("wardrobe.use"))
						p.openInventory(WardrobeManager.getWardrobe(p, 1));
				}
				if (args.length == 1) {
					if (args[0].equalsIgnoreCase("help")) {
						p.sendMessage("§7**********************");
						p.sendMessage(" §eVersion: §a" + getDescription().getVersion());
						p.sendMessage(" §eAuthor: §aTeeage");
						p.sendMessage("§7**********************");
					} else if (args[0].equalsIgnoreCase("item")) {
						if (p.hasPermission("wardrobe.use")) {
							@SuppressWarnings("deprecation")
							Item inventar = new Item(Material.getMaterial(itemId));
							inventar.setData(itemData);
							inventar.setName(ChatColor.translateAlternateColorCodes('&', itemName));
							p.getInventory().setItem(itemSlot, inventar.getItem());
							p.updateInventory();
						}
					}

				}
			}
		}

		return true;
	}

	public static Wardrobe getInstance() {
		return instance;
	}

	public String getItemName() {
		return ChatColor.translateAlternateColorCodes('&', itemName);
	}

	public int getItemId() {
		return itemId;
	}

	public int getItemData() {
		return itemData;
	}

	public int getItemSlot() {
		return itemSlot;
	}

	public List<String> getWorlds() {
		return worlds;
	}

}
