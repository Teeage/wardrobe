package me.teeage.wardrobe;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import me.teeage.wardrobe.util.Item;

public class WardrobeListener implements Listener {

	private Wardrobe plugin = Wardrobe.getInstance();

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		if (p.hasPermission("wardrobe.use")) {
			for (String w : plugin.getWorlds()) {
				World world = Bukkit.getWorld(w);
				if (world == null)
					continue;
				if (p.getWorld().equals(world)) {
					@SuppressWarnings("deprecation")
					Item inventar = new Item(Material.getMaterial(plugin.getItemId()));
					inventar.setData(plugin.getItemData());
					inventar.setName(plugin.getItemName());
					p.getInventory().setItem(plugin.getItemSlot(), inventar.getItem());
					p.updateInventory();
				}
			}
		}

	}

	@EventHandler
	public void onIventoryClick(InventoryClickEvent e) {
		if (e.getCurrentItem() == null || e.getCurrentItem().getItemMeta() == null
				|| e.getInventory().getName() == null)
			return;
		Player p = (Player) e.getWhoClicked();
		if (p.hasPermission("wardrobe.use")) {
			if (e.getInventory().getName().equalsIgnoreCase(WardrobeManager.inv1Name)) {
				e.setCancelled(true);
				if (e.getCurrentItem().getItemMeta().getDisplayName() != null) {
					if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aNext page")) {
						p.openInventory(WardrobeManager.getWardrobe(p, 2));
					}
					if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§eClear Armor")) {
						p.getInventory().setArmorContents(null);
					}
					return;
				}
				WardrobeItem wi = WardrobeManager.getMap(1).get(e.getSlot());
				switch (wi.getMaterial()) {
				case LEATHER_HELMET:
					p.getInventory().setHelmet(wi.getItemStack());
					break;
				case LEATHER_CHESTPLATE:
					p.getInventory().setChestplate(wi.getItemStack());
					break;
				case LEATHER_LEGGINGS:
					p.getInventory().setLeggings(wi.getItemStack());
					break;
				case LEATHER_BOOTS:
					p.getInventory().setBoots(wi.getItemStack());
					break;
				default:
					break;
				}
				p.updateInventory();
				return;

			} else if (e.getInventory().getName().equalsIgnoreCase(WardrobeManager.inv2Name)) {
				e.setCancelled(true);
				if (e.getCurrentItem().getItemMeta().getDisplayName() != null) {
					if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aPrevious page")) {
						p.openInventory(WardrobeManager.getWardrobe(p, 1));
					}
					if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§eClear Armor")) {
						p.getInventory().setArmorContents(null);
					}
					return;
				}
				WardrobeItem wi = WardrobeManager.getMap(2).get(e.getSlot());
				switch (wi.getMaterial()) {
				case LEATHER_HELMET:
					p.getInventory().setHelmet(wi.getItemStack());
					break;
				case LEATHER_CHESTPLATE:
					p.getInventory().setChestplate(wi.getItemStack());
					break;
				case LEATHER_LEGGINGS:
					p.getInventory().setLeggings(wi.getItemStack());
					break;
				case LEATHER_BOOTS:
					p.getInventory().setBoots(wi.getItemStack());
					break;
				default:
					return;
				}
				p.updateInventory();
				return;
			}
		}
		for (WardrobeItem wi : WardrobeManager.getMap(1).values()) {
			if (e.getCurrentItem().isSimilar(wi.getItemStack()))
				e.setCancelled(true);
		}
		for (WardrobeItem wi : WardrobeManager.getMap(2).values()) {
			if (e.getCurrentItem().isSimilar(wi.getItemStack()))
				e.setCancelled(true);
		}
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();

		if (p.hasPermission("wardrobe.sign")) {
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (e.getClickedBlock().getState() instanceof Sign) {
					Sign s = (Sign) e.getClickedBlock().getState();

					if (s.getLine(1).equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("signLineTwo")))) {
						p.performCommand("Wardrobe");
					}

				}
			}
		}
		if (e.getItem() == null || e.getItem().getItemMeta() == null
				|| e.getItem().getItemMeta().getDisplayName() == null)
			return;
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.getItemName())) {
				if (p.hasPermission("wardrobe.use"))
					p.openInventory(WardrobeManager.getWardrobe(p, 1));

			}
		}
	}

	@EventHandler
	public void onSignChange(SignChangeEvent e) {
		if (e.getPlayer().hasPermission("wardrobe.admin")) {
			if (e.getLine(1).equalsIgnoreCase("[wardrobe]")) {
				e.setLine(0, ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("signLineOne")));
				e.setLine(1, ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("signLineTwo")));
				e.setLine(2, ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("signLineThree")));
				e.setLine(3, ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("signLineFour")));
			}
		}

	}
}
