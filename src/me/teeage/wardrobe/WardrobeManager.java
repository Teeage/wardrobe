package me.teeage.wardrobe;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import me.teeage.wardrobe.util.Item;

public class WardrobeManager {

	private final static Wardrobe plugin = Wardrobe.getInstance();

	private final static Map<Integer, WardrobeItem> page1 = new HashMap<>();
	private final static Map<Integer, WardrobeItem> page2 = new HashMap<>();

	public final static String inv1Name = ChatColor.translateAlternateColorCodes('&',
			plugin.getConfig().getString("inv1Name"));
	public final static String inv2Name = ChatColor.translateAlternateColorCodes('&',
			plugin.getConfig().getString("inv2Name"));

	public static void init() {
		// Helmet
		page1.put(0, new WardrobeItem(Material.LEATHER_HELMET, Color.RED, 0));
		page1.put(1, new WardrobeItem(Material.LEATHER_HELMET, Color.BLUE, 1));
		page1.put(2, new WardrobeItem(Material.LEATHER_HELMET, Color.YELLOW, 2));
		page1.put(3, new WardrobeItem(Material.LEATHER_HELMET, Color.GREEN, 3));
		page1.put(4, new WardrobeItem(Material.LEATHER_HELMET, Color.WHITE, 4));
		page1.put(5, new WardrobeItem(Material.LEATHER_HELMET, Color.BLACK, 5));
		page1.put(6, new WardrobeItem(Material.LEATHER_HELMET, Color.PURPLE, 6));
		page1.put(7, new WardrobeItem(Material.LEATHER_HELMET, Color.ORANGE, 7));
		page1.put(8, new WardrobeItem(Material.LEATHER_HELMET, Color.GRAY, 8));

		page2.put(0, new WardrobeItem(Material.LEATHER_HELMET, Color.AQUA, 0));
		page2.put(1, new WardrobeItem(Material.LEATHER_HELMET, Color.FUCHSIA, 1));
		page2.put(2, new WardrobeItem(Material.LEATHER_HELMET, Color.LIME, 2));
		page2.put(3, new WardrobeItem(Material.LEATHER_HELMET, Color.MAROON, 3));
		page2.put(4, new WardrobeItem(Material.LEATHER_HELMET, Color.NAVY, 4));
		page2.put(5, new WardrobeItem(Material.LEATHER_HELMET, Color.OLIVE, 5));
		page2.put(6, new WardrobeItem(Material.LEATHER_HELMET, Color.SILVER, 6));
		page2.put(7, new WardrobeItem(Material.LEATHER_HELMET, Color.TEAL, 7));
		page2.put(8, new WardrobeItem(Material.LEATHER_HELMET, null, 8));
		// Chestplate
		page1.put(9, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.RED, 9));
		page1.put(10, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.BLUE, 10));
		page1.put(11, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.YELLOW, 11));
		page1.put(12, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.GREEN, 12));
		page1.put(13, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.WHITE, 13));
		page1.put(14, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.BLACK, 14));
		page1.put(15, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.PURPLE, 15));
		page1.put(16, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.ORANGE, 16));
		page1.put(17, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.GRAY, 17));

		page2.put(9, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.AQUA, 9));
		page2.put(10, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.FUCHSIA, 10));
		page2.put(11, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.LIME, 11));
		page2.put(12, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.MAROON, 12));
		page2.put(13, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.NAVY, 13));
		page2.put(14, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.OLIVE, 14));
		page2.put(15, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.SILVER, 15));
		page2.put(16, new WardrobeItem(Material.LEATHER_CHESTPLATE, Color.TEAL, 16));
		page2.put(17, new WardrobeItem(Material.LEATHER_CHESTPLATE, null, 17));
		// Leggings
		page1.put(18, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.RED, 18));
		page1.put(19, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.BLUE, 19));
		page1.put(20, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.YELLOW, 20));
		page1.put(21, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.GREEN, 21));
		page1.put(22, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.WHITE, 22));
		page1.put(23, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.BLACK, 23));
		page1.put(24, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.PURPLE, 24));
		page1.put(25, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.ORANGE, 25));
		page1.put(26, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.GRAY, 26));

		page2.put(18, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.AQUA, 18));
		page2.put(19, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.FUCHSIA, 19));
		page2.put(20, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.LIME, 20));
		page2.put(21, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.MAROON, 21));
		page2.put(22, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.NAVY, 22));
		page2.put(23, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.OLIVE, 23));
		page2.put(24, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.SILVER, 24));
		page2.put(25, new WardrobeItem(Material.LEATHER_LEGGINGS, Color.TEAL, 25));
		page2.put(26, new WardrobeItem(Material.LEATHER_LEGGINGS, null, 26));
		// Boots
		page1.put(27, new WardrobeItem(Material.LEATHER_BOOTS, Color.RED, 27));
		page1.put(28, new WardrobeItem(Material.LEATHER_BOOTS, Color.BLUE, 28));
		page1.put(29, new WardrobeItem(Material.LEATHER_BOOTS, Color.YELLOW, 29));
		page1.put(30, new WardrobeItem(Material.LEATHER_BOOTS, Color.GREEN, 30));
		page1.put(31, new WardrobeItem(Material.LEATHER_BOOTS, Color.WHITE, 31));
		page1.put(32, new WardrobeItem(Material.LEATHER_BOOTS, Color.BLACK, 32));
		page1.put(33, new WardrobeItem(Material.LEATHER_BOOTS, Color.PURPLE, 33));
		page1.put(34, new WardrobeItem(Material.LEATHER_BOOTS, Color.ORANGE, 34));
		page1.put(35, new WardrobeItem(Material.LEATHER_BOOTS, Color.GRAY, 35));

		page2.put(27, new WardrobeItem(Material.LEATHER_BOOTS, Color.AQUA, 27));
		page2.put(28, new WardrobeItem(Material.LEATHER_BOOTS, Color.FUCHSIA, 28));
		page2.put(29, new WardrobeItem(Material.LEATHER_BOOTS, Color.LIME, 29));
		page2.put(30, new WardrobeItem(Material.LEATHER_BOOTS, Color.MAROON, 30));
		page2.put(31, new WardrobeItem(Material.LEATHER_BOOTS, Color.NAVY, 31));
		page2.put(32, new WardrobeItem(Material.LEATHER_BOOTS, Color.OLIVE, 32));
		page2.put(33, new WardrobeItem(Material.LEATHER_BOOTS, Color.SILVER, 33));
		page2.put(34, new WardrobeItem(Material.LEATHER_BOOTS, Color.TEAL, 34));
		page2.put(35, new WardrobeItem(Material.LEATHER_BOOTS, null, 35));
	}

	public static Inventory getWardrobe(Player player, int page) {
		Inventory inventory = Bukkit.createInventory(null, 54, getName(page));
		Item item = new Item(Material.STAINED_GLASS_PANE, 1, 7);
		item.setName(" ");
		for (int x = 0; x < inventory.getSize(); x++) {
			inventory.setItem(x, item.getItem());
		}
		for (WardrobeItem wi : getMap(page).values()) {
			inventory.setItem(wi.getSlot(), wi.getItemStack());
		}
		int slot = 45;
		if (page == 1) {
			Item page2 = new Item(Material.REDSTONE);
			page2.setName("§aNext page");
			inventory.setItem(53, page2.getItem());

			slot = 45;
		} else if (page == 2) {
			Item page1 = new Item(Material.REDSTONE);
			page1.setName("§aPrevious page");
			inventory.setItem(45, page1.getItem());

			slot = 53;
		}
		Item clear = new Item(Material.SKULL_ITEM, 1);
		clear.setData(3);
		clear.setSkullOwner(player.getName());
		clear.setName("§eClear Armor");
		inventory.setItem(slot, clear.getItem());
		return inventory;
	}

	public static int getPage(String s) {
		if (s.contains(Integer.valueOf(1).toString()))
			return 1;
		else if (s.contains(Integer.valueOf(2).toString()))
			return 2;
		else
			return 0;
	}

	public static Map<Integer, WardrobeItem> getMap(int page) {
		if (page == 1)
			return page1;
		else if (page == 2)
			return page2;
		return null;

	}

	private static String getName(int page) {
		if (page == 1)
			return inv1Name;
		if (page == 2)
			return inv2Name;
		return inv1Name;
	}
}
