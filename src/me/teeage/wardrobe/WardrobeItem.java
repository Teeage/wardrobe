package me.teeage.wardrobe;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.teeage.wardrobe.util.Item;

public class WardrobeItem {

	private final Color color;
	private final Material material;
	private final int slot;

	public WardrobeItem(Material material, Color color, int slot) {
		this.color = color;
		this.material = material;
		this.slot = slot;
	}

	public ItemStack getItemStack() {
		Item i = new Item(material);
		if(color != null)
			i.setLeatherColor(color);
		return i.getItem();
	}

	public Color getColor() {
		return color;
	}

	public Material getMaterial() {
		return material;
	}

	public int getSlot() {
		return slot;
	}

}
